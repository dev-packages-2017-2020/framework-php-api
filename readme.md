# Introduction

This is a micro PHP Framework, oriented in REST JSON API. 

It provides a small routing system and a system of models that facilitate the documentation in openapi 3 format.

# Getting started

Create a composer.json file with the minimal requirements :

```json
{
  "require": {
    "php": ">=7.4.0",
    "ext-json": "*",
    "ext-pdo": "*",
    "celinederoland/framework-php-api": "*"
  }
}
``` 

Write an .env file :

```env
APP_ENV=devtest

RESOURCES_DIRECTORY=/app/resources/
MODEL_DIRECTORIES=/app/src/Models/
ROUTE_DIRECTORIES=/app/src/Routing/

DB_HOST=framework-php-api-bdd-tests
DB_NAME=ApiTestDatabase
DB_USER=root
DB_PASS=secret

API_TITLE=
API_VERSION=
API_CONTACT_NAME=
API_CONTACT_URL=
API_CONTACT_MAIL=
API_SERVER_URL=
API_SERVER_DESCRIPTION=

# OPTIONAL :

GITLAB_KEY=
GITLAB_URL=
GITLAB_RECURSIVE_DIRECTORIES=/app/src,/app/doc,/app/scripts,/app/tests
GITLAB_FLAT_DIRECTORIES=/app/

REDIS_HOST=redis
REDIS_PORT=6379
```

Generate routes and documentation :

```bash
php vendor/bin/fpa.php
```

Optional : convert @TODO to gitlab issue (the @TODO must me formatted : @TODO -- comma separated tags list -- issue description)

```bash
php vendor/bin/fpa.php generate-gitlab-issues [--debug]
```

// @issue https://gitlab.com/dev-packages-2017-2020/framework-php-api/-/issues/4 -- Doc -- Complete Readme File
// @issue https://gitlab.com/dev-packages-2017-2020/framework-php-api/-/issues/5 -- Integration -- PHP Versions compatibility