#!/usr/bin/env bash

## @issue https://gitlab.com/dev-packages-2017-2020/framework-php-api/-/issues/7 -- DevOps -- php-cs-fixer does not parse all my PHP 7.4 files ?

. env-local
docker-compose -p framework-php-api run --rm framework-php-api-php-cs-fixer fix src
docker-compose -p framework-php-api run --rm framework-php-api-php-cs-fixer fix tests