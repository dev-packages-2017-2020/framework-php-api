<?php

if (file_exists(__DIR__ . '/../../../../autoload.php')) {
    require_once __DIR__ . '/../../../../autoload.php';
} else {
    require_once __DIR__ . '/../../vendor/autoload.php';
}

use Api\DevOps\Documentation;
use Api\DevOps\GitLabIssues;
use Api\DevOps\GitLabIssuesDebug;
use Api\Routing\Router;

$options = getopt("", ["debug"]);
$debug   = isset($options['debug']) && boolval($options['debug']);

array_shift($argv);
$arguments = array_filter($argv, function ($arg) {
    return $arg != "--debug";
});

if (empty($arguments)) {
    $arguments = ['generate-doc', 'generate-route'];
}

foreach ($arguments as $argument) {
    switch ($argument) {
        case 'generate-doc':
            $documentation = Documentation::generate(explode(',', getenv('MODEL_DIRECTORIES')), explode(',', getenv('ROUTE_DIRECTORIES')));
            echo $documentation;
            if (!$debug) {
                file_put_contents(rtrim(getenv('RESOURCES_DIRECTORY'), '/') . '/documentation.yml', $documentation);
            }
            break;
        case 'generate-route':
            $routes = Router::generate(explode(',', getenv('ROUTE_DIRECTORIES')));
            echo $routes;
            if (!$debug) {
                file_put_contents(rtrim(getenv('RESOURCES_DIRECTORY'), '/') . '/routes.yml', $routes);
            }
            break;
        case 'generate-gitlab-issues':
            if ($debug) {
                GitLabIssuesDebug::generate(explode(',', getenv('GITLAB_RECURSIVE_DIRECTORIES')), explode(',', getenv('GITLAB_FLAT_DIRECTORIES')));
            } else {
                GitLabIssues::generate(explode(',', getenv('GITLAB_RECURSIVE_DIRECTORIES')), explode(',', getenv('GITLAB_FLAT_DIRECTORIES')));
            }
            break;
        default:
            echo 'UNEXPECTED ARGUMENT : ' . $argument . PHP_EOL;
            break;
    }
}
