<?php
if (file_exists(__DIR__ . '/../../../../autoload.php')) {
    require_once __DIR__ . '/../../../../autoload.php';
} else {
    require_once __DIR__ . '/../../vendor/autoload.php';
}

use Api\DevOps\Documentation;

$documentation = Documentation::generateNew(explode(',', getenv('MODEL_DIRECTORIES'))/*, explode(',', getenv('ROUTE_DIRECTORIES'))*/);
echo $documentation;
file_put_contents(rtrim(getenv('RESOURCES_DIRECTORY'), '/') . '/openapi/example.yml', $documentation);
