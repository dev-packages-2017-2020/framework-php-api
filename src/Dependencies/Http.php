<?php

namespace Api\Dependencies;

use JsonSerializable;

/**
 * Class Http
 *
 * @package Api\Dependencies
 */
class Http implements IHttp
{
    public function headerAccessControl()
    {
        header('Access-Control-Allow-Methods: HEAD, GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type');
    }

    public function headerJson()
    {
        header("Content-type:application/json");
    }

    public function headerYaml()
    {
        header("Content-type:text/yaml");
    }

    /**
     * @inheritDoc
     */
    public function setResponseCode(int $code)
    {
        http_response_code($code);
    }

    /**
     * @inheritDoc
     */
    public function displayResponseFile(string $fileName)
    {
        echo file_get_contents($fileName);
    }

    /**
     * @inheritDoc
     */
    public function displayResponseJson(JsonSerializable $json)
    {
        echo json_encode($json);
    }
}
