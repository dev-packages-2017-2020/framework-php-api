<?php

namespace Api\Dependencies;

use JsonSerializable;

/**
 * Interface IHttp
 *
 * @package Api\Dependencies
 */
interface IHttp
{
    public function headerAccessControl();

    public function headerJson();

    public function headerYaml();

    /**
     * @param int $code
     *
     * @return void
     */
    public function setResponseCode(int $code);

    /**
     * @param string $fileName
     *
     * @return void
     */
    public function displayResponseFile(string $fileName);

    /**
     * @param JsonSerializable $json
     *
     * @return void
     */
    public function displayResponseJson(JsonSerializable $json);
}
