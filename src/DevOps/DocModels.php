<?php

namespace Api\DevOps;

use Api\NewOA\Model;
use Api\OpenAPI\Decoration\TagObject;
use Api\OpenAPI\Decoration\XTagGroupObject;
use Api\Tools\DirectoryVisitor;
use Api\Tools\FileParser;
use ReflectionClass;

/**
 * Class DocModels
 *
 * @package Api\DevOps
 */
class DocModels
{

    /**
     * @param array $directories
     *
     * @return array
     */
    public static function documentModels(array $directories = []): array
    {
        array_unshift($directories, __DIR__);
        $schemas   = [];
        $tags      = [];
        $tagGroups = [];
        foreach ($directories as $directory) {
            DirectoryVisitor::walkRecursive(
                $directory,
                function ($fileName) use (&$schemas, $directory, &$tags, &$tagGroups) {
                    $namespace = FileParser::parseNamespace($directory, $fileName);
                    list($className, $extension) = explode('.', $fileName, 2);
                    $className = $namespace . $className;
                    if ($extension !== 'php') {
                        return;
                    }
                    $model = self::checkIfThisIsAModel($className);
                    if (is_null($model)) {
                        return;
                    }
                    $schemaName           = str_replace('/', '_', $className);
                    $schema               = $model->buildSchema();
                    $schemas[$schemaName] = $schema;
                    $tag                  = new TagObject();
                    $tag->name            = $schemaName;
                    $tag->description     = '<SchemaDefinition schemaRef="#/components/schemas/' . $schemaName . '"/>';
                    $tags[]               = $tag;
                    if (!is_null($model->tag) && !array_key_exists($model->tag, $tagGroups)) {
                        $tagGroups[$model->tag]       = new XTagGroupObject();
                        $tagGroups[$model->tag]->name = strval($model->tag);
                    }
                    if (!is_null($model->tag)) {
                        $tagGroups[$model->tag]->tags[] = $schemaName;
                    }
                }
            );
        }
        return [$schemas, $tags, $tagGroups];
    }

    public static function tagModels(array $directories = []): array
    {
        array_unshift($directories, __DIR__);
        $schemas = [];
        foreach ($directories as $directory) {
            DirectoryVisitor::walkRecursive($directory, function ($fileName) use (&$schemas, $directory) {
                $namespace = FileParser::parseNamespace($directory, $fileName);
                list($className, $extension) = explode('.', $fileName, 2);
                $className = $namespace . $className;
                if ($extension !== 'php') {
                    return;
                }
                $model = self::checkIfThisIsAModel($className);
                if (is_null($model)) {
                    return;
                }
                $schema                                     = $model->buildSchema();
                $schemas[str_replace('/', '_', $className)] = $schema;
                return;
            });
        }
        return $schemas;
    }

    /**
     * @param string $className
     *
     * @return null|Model
     */
    private static function checkIfThisIsAModel(string $className): ?Model
    {
        if ((new ReflectionClass($className))->isAbstract()) {
            return null;
        }

        /** @var Model $model */
        $model = new $className();
        if (!is_a($model, Model::class)) {
            return null;
        }

        return $model;
    }
}
