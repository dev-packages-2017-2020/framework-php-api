<?php

namespace Api\DevOps;

use Api\Models\Repository;
use Api\OpenAPI\OpenApiConverter;
use Api\OpenAPI\OpenAPIStruct\ComponentsObject;
use Api\OpenAPI\OpenAPIStruct\ContactObject;
use Api\OpenAPI\OpenAPIStruct\InfoObject;
use Api\OpenAPI\OpenAPIStruct\OpenApi;
use Api\OpenAPI\OpenAPIStruct\ServerObject;
use Api\Routing\Router;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Documentation
 *
 * @package Api\DevOps
 */
class Documentation
{
    const TYPE_OBJECT  = 'object';
    const TYPE_STRING  = 'string';
    const TYPE_INTEGER = 'integer';
    const TYPE_NUMBER  = 'number';
    const TYPE_ARRAY   = 'array';

    /**
     * @param array $modelDirectories
     *
     * @return string
     */
    public static function generateNew(array $modelDirectories = []/*, array $routeDirectories = []*/): string
    {
        list($schemas, $tags, $tagGroups) = DocModels::documentModels($modelDirectories);

        $openApi                       = new OpenApi();
        $openApi->info                 = new InfoObject();
        $openApi->info->title          = getenv('API_TITLE');
        $openApi->info->version        = getenv('API_VERSION');
        $openApi->info->description    = getenv('API_DESCRIPTION');
        $openApi->info->contact        = new ContactObject();
        $openApi->info->contact->name  = getenv('API_CONTACT_NAME');
        $openApi->info->contact->url   = getenv('API_CONTACT_URL');
        $openApi->info->contact->email = getenv('API_CONTACT_MAIL');
        $server                        = new ServerObject();
        $server->url                   = getenv('API_SERVER_URL');
        $server->description           = getenv('API_SERVER_DESCRIPTION');
        $openApi->servers              = [$server];
        $openApi->components           = new ComponentsObject();
        $openApi->components->schemas  = $schemas;
        $openApi->tags                 = $tags;
        $openApi->xTagGroups           = $tagGroups;
        return Yaml::dump(OpenApiConverter::convertRec($openApi), 10, 2, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
    }

    /**
     * @param array $modelDirectories
     * @param array $routeDirectories
     *
     * @return string
     */
    public static function generate(array $modelDirectories = [], array $routeDirectories = []): string
    {
        $schemas = Repository::document($modelDirectories);
        $paths   = Router::document($routeDirectories);

        return Yaml::dump([
            'openapi'    => '3.0.0',
            'info'       => [
                'title'       => getenv('API_TITLE'),
                'version'     => getenv('API_VERSION'),
                'contact'     => [
                    'name'  => getenv('API_CONTACT_NAME'),
                    'url'   => getenv('API_CONTACT_URL'),
                    'email' => getenv('API_CONTACT_MAIL'),
                ],
                'description' => 'This is the PostIt API',
            ],
            'servers'    => [
                [
                    'url'         => getenv('API_SERVER_URL'),
                    'description' => getenv('API_SERVER_DESCRIPTION'),
                ],
            ],
            'paths'      => $paths,
            'components' => ['schemas' => $schemas],
        ], 10, 2, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
    }

    /**
     * @param $docComment
     *
     * @return string
     */
    public static function parseDescription(string $docComment): string
    {
        return self::parseMultiline($docComment, 'description');
    }

    /**
     * @param string $docComment
     * @param string $tagName
     *
     * @return string
     */
    private static function parseMultiline(string $docComment, string $tagName): string
    {
        $matches = [];
        $parsed  = [];
        if (preg_match_all('/(@' . $tagName . ') (.*)(\*\/)?$/iUm', $docComment, $matches)) {
            foreach (array_keys($matches[0]) as $key) {
                $parsed[] = trim($matches[2][$key]);
            }
        }
        return implode(PHP_EOL, $parsed);
    }

    /**
     * @param string $docComment
     *
     * @return string
     */
    public static function parseReturn(string $docComment): string
    {
        return self::parseMultiline($docComment, 'return');
    }

    /**
     * @param string $docComment
     *
     * @return string
     */
    public static function parseTitle(string $docComment): string
    {
        return self::parseMultiline($docComment, 'title');
    }

    /**
     * @param string $docComment
     *
     * @return string
     */
    public static function parseTags(string $docComment)
    {
        return self::parseMultiline($docComment, 'tag');
    }

    /**
     * @param $value
     *
     * @return string
     */
    public static function getType($value): string
    {
        if (is_string($value) || is_null($value)) {
            return self::TYPE_STRING;
        }
        if (is_integer($value)) {
            return self::TYPE_INTEGER;
        }
        if (is_float($value)) {
            return self::TYPE_NUMBER;
        }
        if (is_object($value)) {
            return self::TYPE_OBJECT;
        }
        if (is_array($value)) {
            return self::TYPE_ARRAY;
        }
        return self::TYPE_STRING;
    }

    /**
     * @param string $type
     * @param array  $restrict
     *
     * @return string
     */
    public static function restrictTypeTo(string $type, array $restrict = []): string
    {
        if (in_array($type, $restrict)) {
            return $type;
        }
        return self::TYPE_STRING;
    }

    /**
     * @param string $type
     *
     * @return string
     */
    public static function restrictToSimpleType(string $type): string
    {
        return self::restrictTypeTo($type, [self::TYPE_STRING, self::TYPE_INTEGER, self::TYPE_NUMBER]);
    }
}
