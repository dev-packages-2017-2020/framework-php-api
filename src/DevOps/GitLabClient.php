<?php

namespace Api\DevOps;

use Api\Tools\ApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Class GitLabClient
 *
 * @package Api\DevOps
 */
class GitLabClient
{
    private Client $http;
    private string $token;
    private string $url;

    public function __construct()
    {
        $this->http  = new Client();
        $this->token = getenv('GITLAB_KEY');
        $this->url   = getenv('GITLAB_URL');
    }

    /**
     * @param       $method
     * @param       $url
     * @param array $queryParams
     * @param array $bodyParams
     *
     * @return mixed
     */
    public function request($method, $url, $queryParams = [], $bodyParams = [])
    {
        try {
            $response = $this->http->request($method, $this->url . $url, [
                'query'   => $queryParams,
                'json'    => $bodyParams,
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $this->token],
            ]);
        } catch (ClientException $exception) {
            $response = $exception->getResponse();
            throw new ApiException('gitlab_api_error', ApiException::ERROR, 'GitLab Api Error', [
                'body' => $response->getBody()->getContents()
            ], $response->getStatusCode());
        }

        return json_decode($response->getBody()->getContents(), true);
    }
}
