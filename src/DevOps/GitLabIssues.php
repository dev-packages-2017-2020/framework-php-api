<?php

namespace Api\DevOps;

use Api\Tools\DirectoryVisitor;
use Api\Tools\HttpMethods;

/**
 * Class GitLabIssues
 *
 * @package Api\DevOps
 */
class GitLabIssues
{

    /**
     * @param array $recursiveDirectories
     * @param array $flatDirectories
     */
    public static function generate(array $recursiveDirectories = [], array $flatDirectories = []): void
    {
        $gitlab = function (string $fileName, string $directory) {
            $content = file_get_contents($directory . '/' . $fileName);
            $matches = [];
            if (preg_match_all('/^ *(?:\/\/|##|\/\*\*|\*) ?(@TODO)( -- )(.*)( -- )(.*)$/ium', $content, $matches)) {
                foreach ($matches[0] as $key => $match) {
                    $tags    = array_map(function ($t) {
                        return trim($t);
                    }, explode(',', $matches[3][$key]));
                    $title   = '[' . $fileName . '] ' . trim($matches[5][$key], " \t\n\r\0\x0B*/");
                    $content = static::todoToIssue($title, $tags, $match, $content);
                }
                file_put_contents($directory . '/' . $fileName, $content);
            }
        };

        foreach ($recursiveDirectories as $directory) {
            DirectoryVisitor::walkRecursive($directory, $gitlab);
        }
        foreach ($flatDirectories as $directory) {
            DirectoryVisitor::walkFlat($directory, $gitlab);
        }
    }

    /**
     * @param string $title
     * @param array  $tags
     * @param string $match
     * @param string $content
     *
     * @return string
     */
    protected static function todoToIssue(string $title, array $tags, string $match, string $content): string
    {
        $issue = GitLabIssues::createIssue($title, $tags);
        // After creating the issue, we modify the file parsed, so that the T-O-D-O becomes an issue
        $newMatch = str_replace('@TODO', '@issue ' . $issue['web_url'], $match);
        $content  = str_replace($match, $newMatch, $content);
        return $content;
    }

    /**
     * @return mixed
     */
    public static function listIssues()
    {
        $issues = (new GitLabClient())->request(HttpMethods::GET, 'issues');
        return implode(PHP_EOL, array_map(function ($issue) {
            return $issue['title'] . ' --- ' . implode(',', $issue['labels']);
        }, $issues));
    }

    /**
     * @param $title
     * @param $labels
     *
     * @return array
     */
    public static function createIssue($title, $labels)
    {
        return (new GitLabClient())->request(HttpMethods::POST, 'issues', [], ['title' => $title, 'labels' => implode(',', $labels)]);
    }
}
