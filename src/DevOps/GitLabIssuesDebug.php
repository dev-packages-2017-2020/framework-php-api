<?php

namespace Api\DevOps;

/**
 * Class GitLabIssues
 *
 * @package Api\DevOps
 */
class GitLabIssuesDebug extends GitLabIssues
{

    /**
     * @param string $title
     * @param array  $tags
     * @param string $match
     * @param string $content
     *
     * @return string
     */
    protected static function todoToIssue(string $title, array $tags, string $match, string $content): string
    {
        // In debug mode, we do not create the issue and do not modify the file parsed
        echo $title . ' [' . implode(',', $tags) . ']' . PHP_EOL;
        return $content;
    }
}
