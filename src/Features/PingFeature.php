<?php

namespace Api\Features;

use Api\Models\Ping;

/**
 * Class PingFeature
 *
 * @package Api\Features
 */
class PingFeature
{
    public static function ping(Ping $ping): Ping
    {
        return $ping;
    }
}
