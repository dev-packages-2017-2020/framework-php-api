<?php

namespace Api\Models;

/**
 * Class Error
 *
 * @tag     API
 * @package Api\Models
 */
class Error extends Model
{

    /** @description Error Message */
    public ?string $error = '';
    /** @description Error Description */
    public ?string $description = '';
    /** @description File where it happens */
    public ?string $file = '';
    /** @description Line where it happens */
    public ?string $line = '';
    /** @description Severity Level */
    public ?string $level = '';
    /** @description Trace */
    public ?string $trace = '';
    /** @description Additional informations */
    public array $context = [];

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'error'       => strval($this->error),
            'description' => strval($this->description),
            'file'        => strval($this->file),
            'line'        => strval($this->line),
            'level'       => strval($this->level),
            'trace'       => strval($this->trace),
            'context'     => $this->context,
        ];
    }

    /**
     * @param array $input
     * @param array $expectedParameters
     *
     * @return $this
     */
    public function fromInput(array $input, array $expectedParameters = []): Error
    {
        return $this;
    }
}
