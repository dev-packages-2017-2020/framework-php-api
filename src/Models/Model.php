<?php

namespace Api\Models;

use Api\DevOps\Documentation;
use Api\Tools\ValidationException;
use JsonSerializable;
use ReflectionClass;
use ReflectionProperty;

/**
 * Class Model
 *
 * @package Api\Models
 */
abstract class Model implements JsonSerializable
{

    /**
     * @return array[]
     */
    public function document()
    {
        $reflectionClass = new ReflectionClass($this);

        $properties = $reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC);
        $comments   = [];
        foreach ($properties as $property) {
            $comments[$property->getName()] = $property->getDocComment();
        }

        $objectProperties = [];
        foreach ($this->jsonSerialize() as $propertyName => $propertyValue) {
            $type                            = Documentation::getType($propertyValue);
            $objectProperties[$propertyName] = [
                'type' => $type,
            ];
            if ($type === Documentation::TYPE_ARRAY) {
                $objectProperties[$propertyName]['items'] = ['type' => 'string']; // @TODO -- Doc -- Array type
            }
            if (isset($comments[$propertyName]) && $comments[$propertyName]) {
                $objectProperties[$propertyName]['description'] = Documentation::parseDescription($comments[$propertyName]);
            }
        }

        $schemas = [
            $reflectionClass->getShortName() => [
                'title'       => $reflectionClass->getShortName(),
                'type'        => Documentation::TYPE_OBJECT,
                'description' => Documentation::parseDescription($reflectionClass->getDocComment()),
                //'tags'        => [Documentation::parseTags($reflectionClass->getDocComment())],
                'properties'  => $objectProperties,
            ]
        ];

        foreach ($reflectionClass->getMethods(ReflectionProperty::IS_PUBLIC) as $reflectionMethod) {
            if (substr($reflectionMethod->getName(), 0, 4) === 'from') {
                $suffix             = substr($reflectionMethod->getName(), 4);
                $expectedParameters = $reflectionMethod->getParameters()[1];
                $inputProperties    = [];
                foreach ($expectedParameters->getDefaultValue() as $propertyName) {

                    // @issue https://gitlab.com/dev-packages-2017-2020/framework-php-api/-/issues/8 -- Backend, Structure -- if input parameter is not present into output, do something smart
                    $inputProperties[$propertyName] = isset($objectProperties[$propertyName]) ?
                        $objectProperties[$propertyName]
                        : [
                            'type'        => 'string',
                            'description' => $propertyName
                        ];
                }
                $schemas[$reflectionClass->getShortName() . $suffix] = [
                    'title'       => $reflectionClass->getShortName() . $suffix,
                    'type'        => Documentation::TYPE_OBJECT,
                    'description' => Documentation::parseDescription($reflectionMethod->getDocComment()),
                    'properties'  => $inputProperties,
                ];
            }
        }
        return $schemas;
    }

    // @issue https://gitlab.com/dev-packages-2017-2020/framework-php-api/-/issues/9 -- Backend,Structure -- distinguish expected and optional parameters
    // @issue https://gitlab.com/dev-packages-2017-2020/framework-php-api/-/issues/10 -- Backend,Structure -- multiple 'from' methods should be appreciated

    /**
     * @param array    $input
     * @param string[] $expectedParameters
     *
     * @return Model
     */
    abstract public function fromInput(array $input, array $expectedParameters): Model;

    /**
     * @param array $input
     * @param array $expectedParameters
     */
    protected function validate(array $input, array $expectedParameters)
    {
        foreach ($expectedParameters as $key) {
            if (!isset($input[$key])) {
                throw new ValidationException('Parameter ' . $key . ' is missing');
            }
        }
    }
}
