<?php

namespace Api\Models;

use Api\Tools\ApiException;
use JsonSerializable;

/**
 * Class ModelCollection
 *
 * @package Api\Models
 */
class ModelCollection implements JsonSerializable
{
    /** @var Model[] */
    private array $items = [];

    /**
     * ModelCollection constructor.
     *
     * @param array $items
     */
    public function __construct($items = [])
    {
        foreach ($items as $item) {
            if (!($item instanceof Model)) {
                throw new ApiException('invalid model');
            }
        }
        $this->items = $items;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return $this->items;
    }
}