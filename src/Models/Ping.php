<?php

namespace Api\Models;

/**
 * Class Ping
 *
 * @tag     PING
 * @package Api\Models
 */
class Ping extends Model
{
    /** @description */
    public string $response = 'pong';

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'response' => $this->response
        ];
    }

    /**
     * @param array    $input
     * @param string[] $expectedParameters
     *
     * @return $this
     */
    public function fromInput(array $input, array $expectedParameters = ['response']): Ping
    {
        if (isset($input['response'])) {
            $this->response = strval($input['response']);
        }
        return $this;
    }
}
