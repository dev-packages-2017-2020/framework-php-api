<?php

namespace Api\Models;

use Api\Tools\DirectoryVisitor;
use Api\Tools\FileParser;
use ReflectionClass;

/**
 * Class Repository
 *
 * @package Api\Models
 */
class Repository
{

    /**
     * @param array $directories
     *
     * @return array
     */
    public static function document(array $directories = []): array
    {
        array_unshift($directories, __DIR__);
        $schemas = [];
        foreach ($directories as $directory) {
            DirectoryVisitor::walkRecursive($directory, function ($fileName) use (&$schemas, $directory) {
                $namespace = FileParser::parseNamespace($directory, $fileName);
                $model     = self::checkIfThisIsAModel($fileName, $namespace);
                if ($model) {
                    $schemas = array_merge($schemas, $model->document());
                }
                return;
            });
        }
        return $schemas;
    }

    /**
     * @param string $fileName
     *
     * @param string $namespace
     *
     * @return bool|Model
     */
    private static function checkIfThisIsAModel(string $fileName, string $namespace)
    {
        list($className, $extension) = explode('.', $fileName, 2);
        $className = $namespace . $className;
        if ($extension !== 'php') {
            return false;
        }
        if ((new ReflectionClass($className))->isAbstract()) {
            return false;
        }

        /** @var Model $model */
        $model = new $className();
        if (!is_a($model, Model::class)) {
            return false;
        }

        return $model;
    }
}
