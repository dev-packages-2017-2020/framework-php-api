<?php

namespace Api\Models;

/**
 * Class Error
 *
 * @tag     API
 * @package Api\Models
 */
class Success extends Model
{

    /** @description Success Message */
    public string $message = 'success';
    /** @description Additional informations */
    public array $context = [];

    /**
     * Success constructor.
     *
     * @param string $message
     * @param array  $context
     */
    public function __construct(string $message = 'success', array $context = [])
    {
        $this->message = strval($message);
        $this->context = $context;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'success' => $this->message,
            'context' => $this->context,
        ];
    }

    /**
     * @param array $input
     * @param array $expectedParameters
     *
     * @return $this
     */
    public function fromInput(array $input, array $expectedParameters = []): Success
    {
        return $this;
    }
}
