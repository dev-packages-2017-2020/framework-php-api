<?php

namespace Api\NewOA;

use Api\OpenAPI\Schema\BaseArray;
use Api\OpenAPI\Schema\BaseInteger;
use Api\OpenAPI\Schema\BaseString;

/**
 * Class Animal
 *
 * @package Api\NewOA
 */
class Animal extends Model
{
    public BaseInteger $id;
    public BaseString  $name;
    public BaseInteger $paws;
    public BaseString  $diet;
    public BaseString  $specie;
    public BaseArray   $predators;
    public BaseArray   $otherNames;

    public function __construct()
    {
        parent::__construct();
        $this->tag = 'ANIMAL';

        $this->baseObject->title       = 'ANIMAL CLASS';
        $this->baseObject->description = 'This represents an animal';
        $this->baseObject->example     = [
            'id'         => 42,
            'name'       => 'Garfield',
            'paws'       => 4,
            'diet'       => 'carnivorous',
            'specie'     => 'cat',
            'otherNames' => ['the cat', 'felis catus'],
            'predators'  => ['fox']
        ];

        $this->id = new BaseInteger();

        $this->name              = new BaseString();
        $this->name->pattern     = '[a-zA-Z][a-zA-Z_ ]*';
        $this->name->minLength   = 4;
        $this->name->maxLength   = 50;
        $this->name->description = 'BLA BLA BLA';

        $this->paws          = new BaseInteger();
        $this->paws->minimum = 2;
        $this->paws->maximum = 1000;

        $this->diet       = new BaseString();
        $this->diet->enum = ['vegetarian', 'carnivorous', 'omnivorous', 'insectivorous'];

        $this->specie       = new BaseString();
        $this->specie->enum = ['cat', 'dog', 'human', 'ant', 'spider', 'centipede', 'frog', 'fox', 'cow', 'sheep', 'pig', 'wolf'];

        $this->otherNames                     = new BaseArray();
        $this->otherNames->required           = false;
        $this->otherNames->description        = 'OTHER BLA BLA';
        $this->otherNames->items              = new BaseString();
        $this->otherNames->items->pattern     = '[a-zA-Z][a-zA-Z_ ]*';
        $this->otherNames->items->minLength   = 4;
        $this->otherNames->items->maxLength   = 50;
        $this->otherNames->items->description = 'BLA BLA BLA';

        $this->predators              = new BaseArray();
        $this->predators->description = 'The list of animals that eat this animal';
        $this->predators->items       = new BaseString();
        $this->predators->items->enum = ['cat', 'dog', 'human', 'ant', 'spider', 'centipede', 'frog', 'fox', 'cow', 'sheep', 'pig', 'wolf'];
    }

}