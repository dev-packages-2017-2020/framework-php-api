<?php

namespace Api\NewOA;

use Api\OpenAPI\Schema\BaseObject;
use Api\OpenAPI\Schema\SchemaObject;
use ReflectionClass;
use ReflectionProperty;

/**
 * Class Model
 *
 * @package Api\NewOA
 */
class Model
{
    protected BaseObject $baseObject;
    public ?string       $tag = null;

    public function __construct()
    {
        $this->baseObject = new BaseObject();
    }

    public function buildSchema(): BaseObject
    {
        $reflectionClass = new ReflectionClass($this);
        $properties      = $reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC);

        foreach ($properties as $property) {
            if ($property->getValue($this) instanceof SchemaObject) {
                $this->baseObject->properties[$property->getName()] = $property->getValue($this);
            }
        }

        return $this->baseObject;
    }

}