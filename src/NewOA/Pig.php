<?php

namespace Api\NewOA;

/**
 * Class Pig
 *
 * @package Api\NewOA
 */
class Pig extends Animal
{
    public function __construct()
    {
        parent::__construct();
        $this->specie->setValue('pig');
    }
}
