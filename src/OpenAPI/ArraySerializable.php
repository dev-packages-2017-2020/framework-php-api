<?php

namespace Api\OpenAPI;

/**
 * Interface ArraySerializable
 *
 * @package Api\OpenAPI
 */
interface ArraySerializable
{
    public function toArray(): array;
}
