<?php

namespace Api\OpenAPI\Decoration;

use Api\OpenAPI\ArraySerializable;

/**
 * Class ExampleObject
 *
 * @package Api\OpenAPI\Decoration
 */
class ExampleObject implements ArraySerializable
{
    public ?string $summary     = null;
    public ?string $description = null;
    public array   $value;

    // @TODO -- DocFeature -- public ?string $externalValue;

    public function toArray(): array
    {
        return [
            'summary'     => $this->summary,
            'description' => $this->description,
            'value'       => $this->value
        ];
    }
}