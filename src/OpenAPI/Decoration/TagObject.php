<?php

namespace Api\OpenAPI\Decoration;

use Api\OpenAPI\ArraySerializable;

/**
 * Class TagObject
 *
 * @package Api\OpenAPI\Decoration
 */
class TagObject implements ArraySerializable
{
    public string $name;
    public string $description;

    public function toArray(): array
    {
        return [
            'name'        => $this->name,
            'description' => $this->description
        ];
    }
}