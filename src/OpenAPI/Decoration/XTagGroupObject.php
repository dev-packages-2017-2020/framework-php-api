<?php

namespace Api\OpenAPI\Decoration;

use Api\OpenAPI\ArraySerializable;

/**
 * Class XTagGroupObject
 *
 * @package Api\OpenAPI\Decoration
 */
class XTagGroupObject implements ArraySerializable
{

    public string $name;
    public array  $tags;

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'tags' => $this->tags
        ];
    }

}