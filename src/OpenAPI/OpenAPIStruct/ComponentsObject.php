<?php

namespace Api\OpenAPI\OpenAPIStruct;

use Api\OpenAPI\ArraySerializable;
use Api\OpenAPI\Decoration\ExampleObject;
use Api\OpenAPI\Path\HeaderObject;
use Api\OpenAPI\Path\ParameterObject;
use Api\OpenAPI\Path\RequestBodyObject;
use Api\OpenAPI\Path\ResponseObject;
use Api\OpenAPI\Schema\SchemaObject;

/**
 * Class ComponentsObject
 *
 * @package Api\OpenAPI\OpenAPIStruct
 */
class ComponentsObject implements ArraySerializable
{

    /** @var SchemaObject[] */
    public array $schemas;
    /** @var ResponseObject[] */
    public array $responses;
    /** @var ParameterObject[] */
    public array $parameters;
    /** @var ExampleObject[] */
    public array $examples;
    /** @var RequestBodyObject[] */
    public array $requestBodies;
    /** @var HeaderObject[] */
    public array $headers;

    // @TODO -- DocFeature -- CallBackObject
    // @TODO -- DocFeature -- LinkObject
    // @TODO -- DocFeature -- securitySchemes

    public function toArray(): array
    {
        $array = [];
        !empty($this->schemas) && $array['schemas'] = $this->schemas;
        !empty($this->responses) && $array['responses'] = $this->responses;
        !empty($this->parameters) && $array['parameters'] = $this->parameters;
        !empty($this->examples) && $array['examples'] = $this->examples;
        !empty($this->requestBodies) && $array['requestBodies'] = $this->requestBodies;
        !empty($this->headers) && $array['headers'] = $this->headers;
        !empty($this->securitySchemes) && $array['securitySchemes'] = $this->securitySchemes;
        !empty($this->links) && $array['links'] = $this->links;
        return $array;
    }
}