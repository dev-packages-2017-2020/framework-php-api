<?php

namespace Api\OpenAPI\OpenAPIStruct;

use Api\OpenAPI\ArraySerializable;

/**
 * Class ContactObject
 *
 * @package Api\OpenAPI\OpenAPIStruct
 */
class ContactObject implements ArraySerializable
{
    public string  $name;
    public ?string $url   = null;
    public ?string $email = null;

    public function toArray(): array
    {
        $array = ['name' => $this->name,];
        !is_null($this->url) && $array['url'] = $this->url;
        !is_null($this->email) && $array['email'] = $this->email;
        return $array;
    }
}