<?php

namespace Api\OpenAPI\OpenAPIStruct;

use Api\OpenAPI\ArraySerializable;

/**
 * Class InfoObject
 *
 * @package Api\OpenAPI
 */
class InfoObject implements ArraySerializable
{
    public string         $version;
    public string         $title;
    public ?string        $description    = null;
    public ?string        $termsOfService = null;
    public ?ContactObject $contact        = null;
    public ?LicenceObject $license        = null;

    public function toArray(): array
    {
        $array = [
            'version' => $this->version,
            'title'   => $this->title
        ];
        !is_null($this->description) && $array['description'] = $this->description;
        !is_null($this->termsOfService) && $array['termsOfService'] = $this->termsOfService;
        !is_null($this->contact) && $array['contact'] = $this->contact;
        !is_null($this->license) && $array['license'] = $this->license;
        return $array;
    }
}