<?php

namespace Api\OpenAPI\OpenAPIStruct;

use Api\OpenAPI\ArraySerializable;

/**
 * Class LicenceObject
 *
 * @package Api\OpenAPI
 */
class LicenceObject implements ArraySerializable
{
    public string  $name;
    public ?string $url;

    public function toArray(): array
    {
        $array = ['name' => $this->name];
        !is_null($this->url) && $array['url'] = $this->url;
        return $array;
    }
}