<?php

namespace Api\OpenAPI\OpenAPIStruct;

use Api\OpenAPI\ArraySerializable;
use Api\OpenAPI\Decoration\TagObject;
use Api\OpenAPI\Decoration\XTagGroupObject;
use Api\OpenAPI\Path\PathObject;

/**
 * Class OpenApi
 *
 * @package Api\OpenAPI\OpenAPIStruct
 */
class OpenApi implements ArraySerializable
{
    /** @var string */
    public string $openapi = '3.0.3';
    /** @var InfoObject */
    public InfoObject $info;
    /** @var ServerObject[] */
    public array $servers = [];
    /** @var PathObject[] */
    public array            $paths = [];
    public ComponentsObject $components;
    /** @var TagObject[] */
    public array $tags = [];
    /** @var XTagGroupObject[] */
    public array $xTagGroups = [];

    // @TODO -- DocFeature -- security field
    // @TODO -- DocFeature -- externalDocs field
    // @TODO -- DocFeature -- x-tags fields

    public function toArray(): array
    {
        return [
            'openapi'     => $this->openapi,
            'info'        => $this->info,
            'servers'     => $this->servers,
            'tags'        => $this->tags,
            'x-tagGroups' => $this->xTagGroups,
            'paths'       => $this->paths,
            'components'  => $this->components,
        ];
    }
}