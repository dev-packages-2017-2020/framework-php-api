<?php

namespace Api\OpenAPI\OpenAPIStruct;

use Api\OpenAPI\ArraySerializable;

/**
 * Class ServerObject
 *
 * @package Api\OpenAPI\OpenAPIStruct
 */
class ServerObject implements ArraySerializable
{
    public string  $url;
    public ?string $description;

    // @TODO -- DocFeature -- variables field

    public function toArray(): array
    {
        return [
            'url'         => $this->url,
            'description' => $this->description,
        ];
    }
}