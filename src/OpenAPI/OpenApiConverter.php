<?php

namespace Api\OpenAPI;

/**
 * Class OpenApiConverter
 *
 * @package Api\OpenAPI
 */
class OpenApiConverter
{

    /**
     * @param array|ArraySerializable $array
     *
     * @return array
     */
    public static function convertRec($array): array
    {
        if ($array instanceof ArraySerializable) {
            $array = $array->toArray();
        }

        foreach ($array as $key => $value) {
            if ($value instanceof ArraySerializable) {
                $array[$key] = self::convertRec($value->toArray());
                continue;
            }

            if (is_array($value)) {
                $array[$key] = self::convertRec($value);
                continue;
            }

            $array[$key] = $value;
        }
        return $array;
    }
}
