<?php

namespace Api\OpenAPI\Path;

use Api\OpenAPI\ArraySerializable;
use Api\OpenAPI\Schema\SchemaObject;

/**
 * Class HeaderObject
 *
 * @package Api\OpenAPI\Path
 */
class HeaderObject implements ArraySerializable
{

    public ?string         $description;
    public bool            $required = true;
    public SchemaObject    $schema;

    public function toArray(): array
    {
        return [
            'description' => $this->description,
            'required'    => $this->required,
            'schema'      => $this->schema,
        ];
    }
}