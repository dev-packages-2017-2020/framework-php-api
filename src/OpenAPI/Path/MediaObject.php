<?php

namespace Api\OpenAPI\Path;

use Api\OpenAPI\ArraySerializable;
use Api\OpenAPI\Decoration\ExampleObject;
use Api\OpenAPI\Schema\SchemaObject;

/**
 * Class MediaObject
 *
 * @package Api\OpenAPI\Path
 */
class MediaObject implements ArraySerializable
{
    public SchemaObject $schema;
    public ?array       $example;
    /** @var ExampleObject[] */
    public array $examples = [];

    // @TODO -- DocFeature -- encoding

    public function toArray(): array
    {
        $array = [
            'schema' => $this->schema,
        ];
        !is_null($this->example) && $array['example'] = $this->example;
        !empty($this->examples) && $array['examples'] = $this->examples;
        return $array;
    }
}