<?php

namespace Api\OpenAPI\Path;

use Api\OpenAPI\ArraySerializable;

/**
 * Class OperationObject
 *
 * @package Api\OpenAPI
 */
class OperationObject implements ArraySerializable
{

    public array  $tags = [];
    public string $summary;
    public string $description;
    public string $operationId;
    /** @var ParameterObject[] */
    public array              $parameters;
    public ?RequestBodyObject $requestBody;
    /** @var ResponseObject[] */
    public array $responses;

    // @TODO -- DocFeature -- ExternalDocs
    // @TODO -- DocFeature -- Callbacks
    // @TODO -- DocFeature -- deprecated
    // @TODO -- DocFeature -- security
    // @TODO -- DocFeature -- servers

    public function toArray(): array
    {
        $array = [
            'operationId' => $this->operationId,
            'summary'     => $this->summary,
            'description' => $this->description,
            'parameters'  => $this->parameters,
        ];
        !empty($this->tags) && $array['tags'] = $this->tags;
        !is_null($this->requestBody) && $array['requestBody'] = $this->requestBody;
        $array['responses'] = $this->responses;
        return $array;
    }
}