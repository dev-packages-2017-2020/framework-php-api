<?php

namespace Api\OpenAPI\Path;

use Api\OpenAPI\ArraySerializable;
use Api\OpenAPI\Schema\SchemaObject;

/**
 * Class ParameterObject
 *
 * @package Api\OpenAPI\Path
 */
class ParameterObject implements ArraySerializable
{
    const PARAM_IN_QUERY = 0;
    const PARAM_IN_PATH  = 1;
    /*
     * @TODO -- DocFeature -- Params in cookies and headers
    const PARAM_IN_COOKIE = 2;
    const PARAM_IN_HEADER = 3;
     */

    public string          $name;
    public int             $location;
    public ?string         $description;
    public bool            $required = true;
    public SchemaObject    $schema;

    /*
     * @TODO -- DocFeature -- Params deprecated/empty
    public bool $deprecated = false;
    public bool $allowEmptyValue = false;
     */

    public static function location(int $int): string
    {
        switch ($int) {
            case self::PARAM_IN_PATH:
                return 'path';
            case self::PARAM_IN_QUERY:
            default:
                return 'query';
        }
    }

    public function toArray(): array
    {
        return [
            'name'        => $this->name,
            'in'          => self::location($this->location),
            'description' => $this->description,
            'required'    => $this->required,
            'schema'      => $this->schema,
        ];
    }
}