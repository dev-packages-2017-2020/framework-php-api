<?php

namespace Api\OpenAPI\Path;

use Api\OpenAPI\ArraySerializable;

/**
 * Class PathObject
 *
 * @package Api\OpenAPI\Path
 */
class PathObject implements ArraySerializable
{

    public ?string          $summary;
    public ?string          $description;
    public ?OperationObject $get;
    public ?OperationObject $post;
    public ?OperationObject $put;
    public ?OperationObject $delete;
    /** @var ParameterObject[] */
    public array $parameters;

    // @TODO -- DocFeature -- methods options,head,patch,trace
    // @TODO -- DocFeature -- servers field

    public function toArray(): array
    {
        $array = [];
        !is_null($this->summary) && $array['summary'] = $this->summary;
        !is_null($this->description) && $array['description'] = $this->description;
        !empty($this->parameters)
        && $array['parameters'] = $this->parameters;
        !is_null($this->get) && $array['get'] = $this->get;
        !is_null($this->post) && $array['post'] = $this->post;
        !is_null($this->put) && $array['put'] = $this->put;
        !is_null($this->delete) && $array['delete'] = $this->delete;
        return $array;
    }
}