<?php

namespace Api\OpenAPI\Path;

use Api\OpenAPI\ArraySerializable;

/**
 * Class RequestBodyObject
 *
 * @package Api\OpenAPI\Path
 */
class RequestBodyObject implements ArraySerializable
{

    public string $description;
    /** @var MediaObject[] */
    public array $content;
    public bool  $required = true;

    public function toArray(): array
    {
        return [
            'description' => $this->description,
            'content'     => $this->content,
            'required'    => $this->required
        ];
    }
}