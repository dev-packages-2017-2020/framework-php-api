<?php

namespace Api\OpenAPI\Path;

use Api\OpenAPI\ArraySerializable;

/**
 * Class ResponseObject
 *
 * @package Api\OpenAPI\Path
 */
class ResponseObject implements ArraySerializable
{

    public string $description;
    /** @var HeaderObject[] */
    public array $headers = [];
    /** @var MediaObject[] */
    public array $content = [];

    // @TODO -- DocFeature -- links

    public function toArray(): array
    {
        $array = [
            'description' => $this->description,
            'content'     => $this->content,
        ];
        !empty($this->headers) && $array['headers'] = $this->headers;
        return $array;
    }
}