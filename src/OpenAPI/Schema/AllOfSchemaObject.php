<?php

namespace Api\OpenAPI\Schema;

/**
 * Class BBBAllOfSchema
 *
 * @package Api\OpenAPI\Schema
 */
class AllOfSchemaObject extends SchemaObject
{
    /** @var SchemaObject[] */
    public array $schemas;

    public function toArray(): array
    {
        return ['allOf' => $this->schemas];
    }

    /**
     * @inheritDoc
     */
    public function setValue($value): void
    {
        // TODO: Implement setValue() method.
    }
}