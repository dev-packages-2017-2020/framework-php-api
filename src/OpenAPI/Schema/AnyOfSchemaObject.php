<?php

namespace Api\OpenAPI\Schema;

/**
 * Class BBBAnyOfSchema
 *
 * @package Api\OpenAPI\Schema
 */
class AnyOfSchemaObject extends SchemaObject
{
    /** @var SchemaObject[] */
    public array $schemas;

    public function toArray(): array
    {
        return ['anyOf' => $this->schemas];
    }

    /**
     * @inheritDoc
     */
    public function setValue($value): void
    {
        // TODO: Implement setValue() method.
    }
}