<?php

namespace Api\OpenAPI\Schema;

/**
 * Class BaseAnyType
 *
 * @package Api\OpenAPI\Schema
 */
class BaseAnyType extends SchemaObject
{
    public function toArray(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function setValue($value): void
    {
    }
}
