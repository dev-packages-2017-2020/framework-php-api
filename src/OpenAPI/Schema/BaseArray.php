<?php

namespace Api\OpenAPI\Schema;

use Api\Tools\ValidationException;

/**
 * Class BaseArray
 *
 * @package Api\OpenAPI\Schema
 */
class BaseArray extends SchemaObject
{
    public string       $type        = 'array';
    public ?int         $maxItems    = null;
    public ?int         $minItems    = null;
    public ?bool        $uniqueItems = false;
    public SchemaObject $items;

    public function __construct()
    {
        $this->items = new BaseAnyType();
    }

    public function toArray(): array
    {
        $array = parent::toArray();
        if (!is_null($this->maxItems)) {
            $array['maxItems'] = $this->maxItems;
        }
        if (!is_null($this->minItems)) {
            $array['minItems'] = $this->minItems;
        }
        if (!is_null($this->uniqueItems)) {
            $array['uniqueItems'] = $this->uniqueItems;
        }
        $array['items'] = $this->items;
        return $array;
    }

    /**
     * @inheritDoc
     */
    public function setValue($value): void
    {
        if (!is_array($value)) {
            throw new ValidationException();
        }
        if (!is_null($this->minItems) && count($value) < $this->minItems) {
            throw new ValidationException();
        }
        if (!is_null($this->maxItems) && count($value) < $this->maxItems) {
            throw new ValidationException();
        }
        if ($this->uniqueItems && count(array_unique($value)) !== count($value)) {
            throw new ValidationException();
        }
        $this->value = $value;
    }
}