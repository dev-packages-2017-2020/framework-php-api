<?php

namespace Api\OpenAPI\Schema;

/**
 * Class BaseBoolean
 *
 * @package Api\OpenAPI\Schema
 */
class BaseBoolean extends SchemaObject
{
    public string $type = 'boolean';

    /**
     * @inheritDoc
     */
    public function setValue($value): void
    {
        $this->value = boolval($value) || $value === 'true';
    }
}