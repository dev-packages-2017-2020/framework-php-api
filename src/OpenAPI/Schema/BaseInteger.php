<?php

namespace Api\OpenAPI\Schema;

use Api\Tools\ValidationException;

/**
 * Class BaseInteger
 *
 * @package Api\OpenAPI\Schema
 */
class BaseInteger extends BaseNumber
{

    public string $type       = 'integer';
    public ?int   $multipleOf = null;

    public function toArray(): array
    {
        $array = parent::toArray();
        if (!is_null($this->multipleOf)) {
            $array['multipleOf'] = $this->multipleOf;
        }
        return $array;
    }

    /**
     * @inheritDoc
     */
    public function setValue($value): void
    {
        $value = intval($value);
        if (!is_null($this->multipleOf) && $value % $this->multipleOf !== 0) {
            throw new ValidationException();
        }
        parent::setValue($value);
    }
}