<?php

namespace Api\OpenAPI\Schema;

use Api\Tools\ValidationException;

/**
 * Class BaseNumber
 *
 * @package Api\OpenAPI\Schema
 */
class BaseNumber extends SchemaObject
{
    public string $type             = 'number';
    public ?int   $maximum          = null;
    public bool   $exclusiveMaximum = false;
    public ?int   $minimum          = null;
    public bool   $exclusiveMinimum = false;

    public function toArray(): array
    {
        $array = parent::toArray();
        if (!is_null($this->maximum)) {
            $array['maximum'] = $this->maximum;
            if ($this->exclusiveMaximum) {
                $array['exclusiveMaximum'] = true;
            }
        }
        if (!is_null($this->minimum)) {
            $array['minimum'] = $this->minimum;
            if ($this->exclusiveMinimum) {
                $array['exclusiveMinimum'] = true;
            }
        }
        return $array;
    }

    /**
     * @inheritDoc
     */
    public function setValue($value): void
    {
        $value = floatval($value);

        $this->checkLessThanMaximum($value);
        $this->checkGreaterThanMinimum($value);

        $this->value = $value;
    }

    /**
     * @param float $value
     *
     * @throws ValidationException
     */
    private function checkLessThanMaximum(float $value): void
    {
        if (!is_null($this->maximum) && $this->exclusiveMaximum && $value >= $this->maximum) {
            throw new ValidationException();
        }
        if (!is_null($this->maximum) && !$this->exclusiveMaximum && $value > $this->maximum) {
            throw new ValidationException();
        }
    }

    /**
     * @param float $value
     *
     * @throws ValidationException
     */
    private function checkGreaterThanMinimum(float $value): void
    {
        if (!is_null($this->minimum) && $this->exclusiveMinimum && $value >= $this->minimum) {
            throw new ValidationException();
        }
        if (!is_null($this->minimum) && !$this->exclusiveMinimum && $value > $this->minimum) {
            throw new ValidationException();
        }
    }
}