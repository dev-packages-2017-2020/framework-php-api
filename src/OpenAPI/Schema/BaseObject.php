<?php

namespace Api\OpenAPI\Schema;

/**
 * Class BaseObject
 *
 * @package Api\OpenAPI\Schema
 */
class BaseObject extends SchemaObject
{
    public string $type          = 'object';
    public ?int   $minProperties = null;
    public ?int   $maxProperties = null;
    /** @var SchemaObject[] (indexed by keys = property-name) */
    public array $properties = [];

    public function toArray(): array
    {
        $array = parent::toArray();
        !is_null($this->minProperties) && $array['minProperties'] = $this->minProperties;
        !is_null($this->maxProperties) && $array['mawProperties'] = $this->maxProperties;
        !empty($this->properties) && $array['properties'] = $this->properties;
        $required = [];
        foreach ($this->properties as $propertyName => $property) {
            if ($property->required) {
                $array['required'][] = $propertyName;
            }
        }
        !empty($required) && $array['required'] = $required;
        return $array;
    }

    /**
     * @inheritDoc
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }
}