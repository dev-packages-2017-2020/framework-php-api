<?php

namespace Api\OpenAPI\Schema;

use Api\Tools\ValidationException;

/**
 * Class BaseString
 *
 * @package Api\OpenAPI\Schema
 */
class BaseString extends SchemaObject
{

    public string  $type      = 'string';
    public ?int    $maxLength = null;
    public ?int    $minLength = null;
    public ?string $pattern   = null;

    public function toArray(): array
    {
        $array = parent::toArray();
        !is_null($this->maxLength) && $array['maxLength'] = $this->maxLength;
        !is_null($this->minLength) && $array['minLength'] = $this->minLength;
        !is_null($this->pattern) && $array['pattern'] = $this->pattern;
        return $array;
    }

    /**
     * @inheritDoc
     */
    public function setValue($value): void
    {
        $value = strval($value);
        if (!is_null($this->minLength) && strlen($value) < $this->minLength) {
            throw new ValidationException();
        }
        if (!is_null($this->maxLength) && strlen($value) > $this->maxLength) {
            throw new ValidationException();
        }
        if (!is_null($this->pattern) && !preg_match('/^' . $this->pattern . '$/', $value)) {
            throw new ValidationException();
        }
        $this->value = $value;
    }
}