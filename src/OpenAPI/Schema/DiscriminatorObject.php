<?php

namespace Api\OpenAPI\Schema;

use Api\OpenAPI\ArraySerializable;

/**
 * Class DiscriminatorObject
 *
 * @package Api\OpenAPI
 */
class DiscriminatorObject implements ArraySerializable
{
    public string $propertyName;
    public array  $mapping = [];

    public function toArray(): array
    {
        $array = [
            'propertyName' => $this->propertyName
        ];
        (!empty($this->mapping)) && $array['mapping'] = $this->mapping;
        return $array;
    }
}