<?php

namespace Api\OpenAPI\Schema;

/**
 * Class BBBOneOfSchema
 *
 * @package Api\OpenAPI
 */
class OneOfSchemaObject extends SchemaObject
{
    /** @var SchemaObject[] */
    public array $schemas;

    public function toArray(): array
    {
        return ['oneOf' => $this->schemas];
    }

    /**
     * @inheritDoc
     */
    public function setValue($value): void
    {
        // TODO: Implement setValue() method.
    }
}