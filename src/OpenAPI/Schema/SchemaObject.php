<?php

namespace Api\OpenAPI\Schema;

use Api\OpenAPI\ArraySerializable;

/**
 * Class BBBSchema
 *
 * @package Api\OpenAPI\Schema
 */
abstract class SchemaObject implements ArraySerializable
{

    public bool   $nullable    = false;
    public ?array $example     = null;
    public string $type        = '';
    public string $title       = '';
    public string $description = '';
    public bool   $required    = true;
    public array  $enum        = [];

    /** @var mixed */
    protected $value;

    public function toArray(): array
    {
        $array = [
            'type'        => $this->type,
            'title'       => $this->title,
            'description' => $this->description,
            'nullable'    => $this->nullable,
            'example'     => $this->example
        ];
        if (!empty($this->enum)) {
            $array['enum'] = $this->enum;
        }
        return $array;
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public abstract function setValue($value): void;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}