<?php

namespace Api\Routing;

use Api\Features\PingFeature;
use Api\Models\Ping;
use Api\Tools\HttpMethods;

/**
 * Class Ping
 *
 * @title       Ping-Pong
 * @description Use this to check if you can communicate with API
 *
 * @tag         PING
 * @package     Api\Routing
 */
class PingRoute extends Route
{
    public function bodyType(): string
    {
        return Ping::class;
    }

    public function method(): string
    {
        return HttpMethods::GET;
    }

    public function path(): string
    {
        return '/ping';
    }

    /**
     * @param Ping $ping
     *
     * @return Ping
     */
    public function execute(Ping $ping): Ping
    {
        return PingFeature::ping($ping);
    }
}
