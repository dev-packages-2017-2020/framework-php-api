<?php

namespace Api\Routing;

use Api\Features\PingFeature;
use Api\Models\Ping;
use Api\Tools\HttpMethods;

/**
 * Class PingWithPathParam
 *
 * @title   Ping-{:Pong}
 * @tag     PING
 *
 * @package Api\Routing
 */
class PingWithPathParam extends Route
{
    public function bodyType(): string
    {
        return Ping::class;
    }

    public function method(): string
    {
        return HttpMethods::GET;
    }

    public function path(): string
    {
        return '/ping/$response:string';
    }

    /**
     * @param string $response
     * @param Ping   $ping
     *
     * @return Ping
     */
    public function execute(string $response, Ping $ping): Ping
    {
        $ping->response = $response;
        return PingFeature::ping($ping);
    }
}
