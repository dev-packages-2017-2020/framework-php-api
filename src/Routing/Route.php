<?php

namespace Api\Routing;

use Api\DevOps\Documentation;
use Api\Models\Model;
use Api\Models\ModelCollection;
use Api\Tools\ForbiddenException;
use Api\Tools\HttpMethods;
use Api\Tools\NotFoundException;
use JsonSerializable;
use ReflectionClass;

/**
 * Class Route
 *
 * @package Api\Routing
 */
abstract class Route
{

    protected array  $query;
    protected string $bearer;

    /**
     * @param array  $pathParams
     * @param array  $query
     * @param array  $body
     * @param string $bearer
     *
     * @return Model|ModelCollection
     */
    public function render($pathParams = [], $query = [], $body = [], string $bearer = ''): JsonSerializable
    {
        $type         = $this->bodyType();
        $body         = empty($type) ? false : (new $type())->fromInput($body);
        $params       = array_values($pathParams);
        $this->query  = $query;
        $this->bearer = $bearer;
        if ($body) {
            $params[] = $body;
        }
        return call_user_func_array([$this, 'execute'], $params);
    }

    abstract public function bodyType(): string;

    /**
     * @return array
     */
    public function document()
    {
        $reflectionClass = new ReflectionClass($this);
        $inputClass      = false;
        if (!empty($this->bodyType())) {
            $inputClass = explode('\\', $this->bodyType());
            $inputClass = end($inputClass);
        }

        $inputPathParams = [];
        $matches         = null;
        preg_match_all('/\$([a-zA-Z\-_0-9]+):(.+)(\/|\b)/U', $this->path(), $matches);
        foreach (array_keys($matches[0]) as $matchKey) {
            $paramName         = $matches[1][$matchKey];
            $paramType         = $matches[2][$matchKey];
            $inputPathParams[] = [
                'name'     => $paramName,
                'in'       => 'path',
                'required' => true,
                'schema'   => ['type' => Documentation::restrictToSimpleType($paramType)]
            ];
        }

        $outputClass = explode('\\', (string) $reflectionClass->getMethod('execute')->getReturnType());
        $outputClass = end($outputClass);

        $outputSchema = ['schema' => ['$ref' => '#/components/schemas/' . $outputClass]];
        if (($outputClass === 'ModelCollection')) {
            $return = Documentation::parseReturn($reflectionClass->getMethod('execute')->getDocComment());
            if (substr($return, -2)) {
                $outputClass = substr($return, 0, -2);
            }
            $outputSchema = ['schema' => ['type' => 'array', 'items' => ['$ref' => '#/components/schemas/' . $outputClass]]];
        }

        $responses = [
            '200' => [
                'description' => 'OK',
                'content'     => [
                    'application/json' => $outputSchema
                ]
            ],
            '403' => [
                'description' => (new ForbiddenException())->getDescription(),
                'content'     => [
                    'application/json' => ['schema' => ['$ref' => '#/components/schemas/Error']]
                ]
            ],
            '404' => [
                'description' => (new NotFoundException())->getDescription(),
                'content'     => [
                    'application/json' => ['schema' => ['$ref' => '#/components/schemas/Error']]
                ]
            ],
            '422' => [
                'description' => (new NotFoundException())->getDescription(),
                'content'     => [
                    'application/json' => ['schema' => ['$ref' => '#/components/schemas/Error']]
                ]
            ]
        ];

        $doc = [
            'operationId' => $this->method() . '-' . str_replace('/', '-', $this->toStringPath()),
            'description' => Documentation::parseDescription($reflectionClass->getDocComment()),
            'summary'     => Documentation::parseTitle($reflectionClass->getDocComment()),
            'tags'        => [Documentation::parseTags($reflectionClass->getDocComment())],
            'responses'   => $responses,
        ];
        if (!empty($inputPathParams)) {
            $doc['parameters'] = $inputPathParams;
        }
        if ($inputClass && $this->method() !== HttpMethods::GET) {
            $doc['requestBody'] = [
                'content' => [
                    'application/json' => ['schema' => ['$ref' => '#/components/schemas/' . $inputClass . 'ControlInputText']],
                ]
            ];
        }
        return $doc;
    }

    abstract public function method(): string;

    public function toStringPath(): string
    {
        return preg_replace('/\$([a-zA-Z\-_0-9]*):.+(\/|\b)/U', '{$1}$2', $this->path());
    }

    abstract public function path(): string;
}
