<?php

namespace Api\Routing;

use Api\Tools\ApiException;
use Api\Tools\DirectoryVisitor;
use Api\Tools\FileParser;
use Api\Tools\Injector;
use Api\Tools\NotFoundException;
use Exception as BaseException;
use ReflectionClass;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Router
 *
 * @package Api\Routing
 */
class Router
{
    /**
     * @param string $method
     * @param string $uri
     * @param array  $query
     * @param array  $body
     * @param string $bearer
     */
    public static function forward(string $method, string $uri, array $query = [], array $body = [], string $bearer = '')
    {
        $requestPath = explode('/', rtrim(parse_url($uri)['path'], '/'));
        array_shift($requestPath);

        if ($requestPath[0] === 'api') {
            Injector::http()->headerAccessControl();

            if ($requestPath[1] === 'doc') {
                Injector::http()->headerYaml();
                Injector::http()->displayResponseFile(getenv('RESOURCES_DIRECTORY') . '/documentation.yml');
                return;
            }

            array_shift($requestPath);
            Injector::http()->headerJson();
            try {
                list($class, $pathParams) = self::match($method, $requestPath);
                if ($class) {
                    /** @var Route $route */
                    $route = new $class();
                    Injector::http()->displayResponseJson($route->render($pathParams, $query, $body, $bearer));
                    return;
                }
                throw new NotFoundException(['method' => $method, 'requestPath' => $requestPath]);
            } catch (ApiException $exception) {
                Injector::http()->setResponseCode($exception->getCode());
                Injector::http()->displayResponseJson($exception);
                return;
            } catch (BaseException $exception) {
                Injector::http()->setResponseCode(500);
                $unexpectedError = new ApiException(
                    'unexpected_error',
                    ApiException::ERROR,
                    $exception->getMessage(),
                    [
                        'file'  => $exception->getFile(),
                        'line'  => $exception->getLine(),
                        'trace' => $exception->getTrace()
                    ]
                );
                Injector::http()->displayResponseJson($unexpectedError);
                return;
            }
        }

        Injector::http()->displayResponseFile(getenv('RESOURCES_DIRECTORY') . '/index.html');
    }

    /**
     * @param string $method
     * @param array  $requestPath
     *
     * @return array|bool[]
     */
    public static function match(string $method, array $requestPath): array
    {
        $routes = Yaml::parseFile(getenv('RESOURCES_DIRECTORY') . '/routes.yml');
        if (!is_array($routes[$method])) {
            return [false, false];
        }
        foreach ($routes[$method] as $registeredRoute => $class) {
            $registeredRoute = explode('/', trim($registeredRoute, '/'));
            $params          = self::matchPath($requestPath, $registeredRoute);
            if (is_array($params)) {
                return [$class, $params];
            }
        }
        return [false, false];
    }

    /**
     * @param array $requestPath
     * @param array $registeredRoute
     *
     * @return array|bool
     */
    public static function matchPath(array $requestPath, array $registeredRoute)
    {
        if (count($requestPath) !== count($registeredRoute)) {
            return false;
        }
        $vars = [];
        foreach (array_keys($requestPath) as $key) {
            if (substr($registeredRoute[$key], 0, 1) !== '$') {
                if ($requestPath[$key] === $registeredRoute[$key]) {
                    continue;
                }
                return false;
            }
            list($varName, $varType) = explode(':', substr($registeredRoute[$key], 1), 2);
            $vars[$varName] = $requestPath[$key];
            switch ($varType) {
                case 'integer':
                    $vars[$varName] = intval($vars[$varName]);
                    break;
                case 'number':
                    $vars[$varName] = floatval($vars[$varName]);
                    break;
                case 'string':
                    $vars[$varName] = strval($vars[$varName]);
                    break;
                default:
                    break;
            }
            // @issue https://gitlab.com/dev-packages-2017-2020/framework-php-api/-/issues/12 -- Backend, Structure -- validate varValue with varType ; validate that varType is a valid OpenApi v3 type
        }
        return $vars;
    }

    /**
     * @param array $directories
     *
     * @return string
     */
    public static function generate(array $directories = []): string
    {
        array_unshift($directories, __DIR__);
        $routes = [];
        foreach ($directories as $directory) {
            DirectoryVisitor::walkRecursive($directory, function ($file) use (&$routes, $directory) {
                $namespace = FileParser::parseNamespace($directory, $file);
                $route     = self::checkThisFileIsARoute($file, $namespace);
                if (!$route) {
                    return;
                }
                if (!isset($routes[$route->method()])) {
                    $routes[$route->method()] = [];
                }
                $routes[$route->method()][$route->path()] = get_class($route);
                return;
            });
        }
        return Yaml::dump($routes, 4, 2);
    }

    /**
     * @param string $file
     * @param string $namespace
     *
     * @return bool|Route
     */
    private static function checkThisFileIsARoute(string $file, string $namespace)
    {
        list($className, $extension) = explode('.', $file, 2);
        $className = $namespace . $className;
        if ($extension !== 'php') {
            return false;
        }
        if ((new ReflectionClass($className))->isAbstract()) {
            return false;
        }
        /** @var Route $route */
        $route = new $className();
        if (!is_a($route, Route::class)) {
            return false;
        }
        return $route;
    }

    /**
     * @param array $directories
     *
     * @return array
     */
    public static function document(array $directories = []): array
    {
        array_unshift($directories, __DIR__);
        $docPaths = [];
        foreach ($directories as $directory) {
            DirectoryVisitor::walkRecursive($directory, function ($file) use (&$docPaths, $directory) {
                $namespace = FileParser::parseNamespace($directory, $file);
                $route     = self::checkThisFileIsARoute($file, $namespace);
                if (!$route) {
                    return;
                }
                if (!isset($docPaths[$route->toStringPath()])) {
                    $docPaths[$route->toStringPath()] = [];
                }
                $docPaths[$route->toStringPath()][strtolower($route->method())] = $route->document();
                return;
            });
        }

        return $docPaths;
    }
}
