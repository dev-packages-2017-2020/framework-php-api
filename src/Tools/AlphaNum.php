<?php

namespace Api\Tools;

/**
 * Class AlphaNum
 *
 * @package Api\Tools
 */
class AlphaNum
{

    /**
     * @param $string
     *
     * @return string|string[]|null
     */
    public static function sanitize($string)
    {
        return preg_replace('/[^a-zA-Z0-9]+/', '', $string);
    }
}
