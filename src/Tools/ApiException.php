<?php

namespace Api\Tools;

use Exception;
use JsonSerializable;

/**
 * Class TrelloKanbanException
 *
 * @package Api\Tools
 */
class ApiException extends Exception implements JsonSerializable
{
    const ERROR      = 0; //Unexpected error, must be thrown and reported
    const MUTE_ERROR = 1; //Expected error, must be thrown but not reported
    const WARNING    = 2; //Must not by thrown but must be reported
    const LOG        = 3; //Must be saved in case we want to explore what happened

    const UNDEFINED_DEPENDENCY = 'undefined_dependency';
    protected ?array $context;
    protected string $description;
    protected int    $level;

    /**
     * TrelloKanbanException constructor.
     *
     * @param string     $message
     * @param int        $level
     * @param string     $description
     * @param array|null $context
     * @param int        $code
     */
    public function __construct($message = 'api_error', $level = self::ERROR, $description = '', $context = [], $code = 500)
    {
        $this->context     = $context;
        $this->description = $description;
        $this->level       = $level;
        parent::__construct($message, $code);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'error'       => $this->getMessage(),
            'description' => $this->getDescription(),
            'file'        => $this->getFile(),
            'line'        => $this->getLine(),
            'level'       => $this->getLevel(),
            'trace'       => $this->getTrace(),
            'context'     => $this->getContext()
        ];
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }
}
