<?php

namespace Api\Tools;

/**
 * Class Config
 *
 * @package Api\Tools
 */
class Config
{
    private static array $database = [];
    private static array $redis    = [];

    /**
     * @return array
     */
    public static function database(): array
    {
        if (empty(self::$database)) {
            self::$database = [
                'host' => getenv('DB_HOST'),
                'name' => getenv('DB_NAME'),
                'user' => getenv('DB_USER'),
                'pass' => getenv('DB_PASS'),
            ];
        }
        return self::$database;
    }

    /**
     * @return array
     */
    public static function redis(): array
    {
        if (empty(self::$redis)) {
            self::$redis = [
                'host' => getenv('REDIS_HOST'),
                'port' => getenv('REDIS_PORT'),
            ];
        }
        return self::$redis;
    }
}
