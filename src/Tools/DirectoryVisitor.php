<?php

namespace Api\Tools;

/**
 * Class DirectoryVisitor
 *
 * @package Api\Tools
 */
class DirectoryVisitor
{

    /**
     * @param string   $directory
     * @param callable $callback function(string $fileName, string $directory): void
     */
    public static function walkFlat(string $directory, callable $callback)
    {
        rtrim($directory, '/');
        $files = scandir($directory);
        foreach ($files as $fileName) {
            if ($fileName !== '.' && $fileName !== '..' && !is_dir($directory . '/' . $fileName)) {
                $callback($fileName, $directory);
            }
        }
    }

    /**
     * @param string   $directory
     * @param callable $callback function(string $fileName, string $directory): void
     */
    public static function walkRecursive(string $directory, callable $callback)
    {
        rtrim($directory, '/');
        $files = scandir($directory);
        foreach ($files as $fileName) {
            if ($fileName !== '.' && $fileName !== '..' && !is_dir($directory . '/' . $fileName)) {
                $callback($fileName, $directory);
            }
            if ($fileName !== '.' && $fileName !== '..' && is_dir($directory . '/' . $fileName)) {
                self::walkRecursive($directory . '/' . $fileName, $callback);
            }
        }
    }
}
