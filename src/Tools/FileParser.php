<?php

namespace Api\Tools;

/**
 * Class FileParser
 *
 * @package Api\Tools
 */
class FileParser
{

    /**
     * @param string $directory
     * @param string $file
     *
     * @return string
     */
    public static function parseNamespace(string $directory, string $file)
    {
        $namespace = '\\';
        $rfile     = fopen($directory . '/' . $file, 'r');
        while (($line = fgets($rfile, 4096)) !== false) {
            $line = trim($line);
            if (substr($line, 0, 9) == 'namespace') {
                $namespace = rtrim(trim(substr($line, 10)), ';') . $namespace;
                fclose($rfile);
                return $namespace;
            }
        }
        fclose($rfile);
        return $namespace;
    }
}
