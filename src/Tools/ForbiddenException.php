<?php

namespace Api\Tools;

/**
 * Class ForbiddenException
 *
 * @package Api\Tools
 */
class ForbiddenException extends ApiException
{
    /**
     * ForbiddenException constructor.
     *
     * @param string $description
     */
    public function __construct(string $description = 'forbidden')
    {
        parent::__construct('forbidden', ApiException::MUTE_ERROR, $description, [], 403);
    }
}
