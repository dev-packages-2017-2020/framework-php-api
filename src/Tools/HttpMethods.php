<?php

namespace Api\Tools;

/**
 * Class HttpMethods
 *
 * @package Api\Tools
 */
class HttpMethods
{
    const GET    = 'GET';
    const POST   = 'POST';
    const PUT    = 'PUT';
    const DELETE = 'DELETE';
}
