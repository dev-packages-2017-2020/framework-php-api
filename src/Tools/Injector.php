<?php

namespace Api\Tools;

use Api\Dependencies\Http;
use Api\Dependencies\IHttp;

/**
 * Class Injector
 *
 * Pattern Dependency injection
 *
 * @package Api\Tools
 */
class Injector
{

    /** @var DB */
    private static ?DB $database = null;
    /** @var IHttp */
    private static ?IHttp $http = null;

    /**
     * @return DB
     */
    public static function database(): DB
    {
        if (!isset(self::$database)) {
            self::$database = new DB();
        }
        return self::$database;
    }

    public static function http(IHttp $http = null): IHttp
    {
        if (!is_null($http)) {
            self::$http = $http;
        }
        if (!isset(self::$http)) {
            self::$http = new Http();
        }
        return self::$http;
    }
}
