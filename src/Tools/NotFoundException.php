<?php

namespace Api\Tools;

/**
 * Class NotFoundException
 *
 * @package Api\Tools
 */
class NotFoundException extends ApiException
{

    /**
     * NotFoundException constructor.
     *
     * @param array|null $context
     */
    public function __construct(?array $context = [])
    {
        parent::__construct('not_found', ApiException::MUTE_ERROR, 'Not Found', $context, 404);
    }
}
