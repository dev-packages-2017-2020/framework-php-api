<?php

namespace Api\Tools;

/**
 * Class ValidationException
 *
 * @package Api\Tools
 */
class ValidationException extends ApiException
{
    /**
     * ValidationException constructor.
     *
     * @param string $description
     */
    public function __construct(string $description = 'Invalid')
    {
        parent::__construct('invalid_parameters', ApiException::MUTE_ERROR, $description, [], 422);
    }
}
