<?php

namespace Api;

/**
 * Class ConfigTest
 *
 * @package Api
 */
class ConfigTest extends TestCase
{
    public function testOk()
    {
        self::assertTrue(true);
        self::assertEquals('devtest', getenv('APP_ENV'));
    }
}
