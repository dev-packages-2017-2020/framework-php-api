<?php

namespace Api\Models;

use Api\TestCase;

/**
 * Class ModelTest
 *
 * @package Api\Models
 */
class ModelTest extends TestCase
{
    public function testDocument()
    {
        $doc = (new Stub())->document();
        self::assertArray([
            'Stub'      =>
                [
                    'title'       => 'Stub',
                    'type'        => 'object',
                    'description' => 'this represents something ...' . PHP_EOL . 'do not forget it plz',
                    'properties'  =>
                        [
                            'uid'   =>
                                [
                                    'type'        => 'string',
                                    'description' => 'A version 4 UUID',
                                ],
                            'name'  =>
                                [
                                    'type'        => 'string',
                                    'description' => 'Must be a valid name',
                                ],
                            'email' =>
                                [
                                    'type'        => 'string',
                                    'description' => 'Must be a valid email',
                                ],
                        ],
                ],
            'StubInput' =>
                [
                    'title'       => 'StubInput',
                    'type'        => 'object',
                    'description' => 'Give me your name and email',
                    'properties'  =>
                        [
                            'name'  =>
                                [
                                    'type'        => 'string',
                                    'description' => 'Must be a valid name',
                                ],
                            'email' =>
                                [
                                    'type'        => 'string',
                                    'description' => 'Must be a valid email',
                                ],
                        ],
                ],
        ], $doc);
    }

    public function testAllDocuments()
    {
        $doc = Repository::document();
        self::assertIsArray($doc);
    }
}
