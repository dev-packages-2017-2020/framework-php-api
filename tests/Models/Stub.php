<?php

namespace Api\Models;

use JsonSerializable;

/**
 * Class Stub
 *
 * @description this represents something ...
 * @description do not forget it plz
 * @tag         UNIT_TEST
 */
class Stub extends Model implements JsonSerializable
{
    public int $id = 0;
    /** @description A version 4 UUID */
    public string $uid = '...';
    /** @description Must be a valid name */
    public string $name = '...';
    /** @description Must be a valid email */
    public string $email = '...';
    public string $hash  = '...';
    public string $token = '...';

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'uid'   => strval($this->uid),
            'name'  => strval($this->name),
            'email' => strval($this->email),
        ];
    }

    /**
     * @param array    $input
     * @param string[] $expectedParameters
     *
     * @description Give me your name and email
     * @return $this
     */
    public function fromInput($input, $expectedParameters = ['name', 'email']): Stub
    {
        $this->validate($input, $expectedParameters);
        $this->name  = strval($input['name']);
        $this->email = strval($input['email']);
        return $this;
    }
}
