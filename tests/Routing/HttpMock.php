<?php

namespace Api\Routing;

use Api\Dependencies\IHttp;
use JsonSerializable;

/**
 * Class HttpMock
 *
 * @package Api\Routing
 */
class HttpMock implements IHttp
{
    public int $responseCode = 200;
    /** @var string|array */
    public      $response         = '';
    public bool $hasAccessControl = false;
    public bool $isJson           = false;
    public bool $isYaml           = false;
    public bool $isHtml           = true;

    public function headerAccessControl()
    {
        $this->hasAccessControl = true;
    }

    public function headerJson()
    {
        $this->isJson = true;
        $this->isYaml = false;
        $this->isHtml = false;
    }

    public function headerYaml()
    {
        $this->isYaml = true;
        $this->isJson = false;
        $this->isHtml = false;
    }

    /**
     * @inheritDoc
     */
    public function setResponseCode(int $code)
    {
        $this->responseCode = $code;
    }

    /**
     * @inheritDoc
     */
    public function displayResponseFile(string $fileName)
    {
        $this->response = file_get_contents($fileName);
    }

    /**
     * @inheritDoc
     */
    public function displayResponseJson(JsonSerializable $json)
    {
        $this->response = json_decode(json_encode($json), true);
    }
}
