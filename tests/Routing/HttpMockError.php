<?php

namespace Api\Routing;

use Api\Tools\ApiException;
use Exception as BaseException;
use JsonSerializable;

/**
 * Class HttpMockError
 *
 * @package Api\Routing
 */
class HttpMockError extends HttpMock
{

    /**
     * @inheritDoc
     */
    public function displayResponseJson(JsonSerializable $json)
    {
        if (!is_a($json, ApiException::class)) {
            throw new BaseException('OUPS!!!');
        }
        parent::displayResponseJson($json);
    }
}
