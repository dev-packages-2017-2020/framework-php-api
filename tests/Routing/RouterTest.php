<?php

namespace Api\Routing;

use Api\Tools\HttpMethods;
use Api\Tools\Injector;

/**
 * Class RouterTest
 *
 * @package Api\Routing
 */
class RouterTest extends RouterTestCase
{
    public function testMatch()
    {
        list($class, $pathParams) = Router::match(HttpMethods::GET, ['ping']);
        self::assertEquals(PingRoute::class, $class);
        self::assertIsArray($pathParams);
        self::assertCount(0, $pathParams);

        list($class, $pathParams) = Router::match(HttpMethods::GET, ['ping', 'tartempion']);
        self::assertEquals(PingWithPathParam::class, $class);
        self::assertIsArray($pathParams);
        self::assertCount(1, $pathParams);
        self::assertEquals('tartempion', $pathParams['response']);
    }

    public function testGenerate()
    {
        $routes = Router::generate();
        self::assertIsString($routes);
    }

    public function testForward()
    {
        $http = $this->execRoute(HttpMethods::GET, '/api/ping');
        self::assertTrue($http->isJson);
        self::assertEquals(200, $http->responseCode);
        self::assertEquals('pong', $http->response['response']);

        $http = $this->execRoute(HttpMethods::GET, '/api/badaboum');
        self::assertTrue($http->isJson);
        self::assertEquals(404, $http->responseCode);

        $http = new HttpMockError();
        Injector::http($http);
        Router::forward(HttpMethods::GET, '/api/ping');
        self::assertTrue($http->isJson);
        self::assertEquals(500, $http->responseCode);

        $http = $this->execRoute(HttpMethods::GET, '/api/doc');
        self::assertTrue($http->isYaml);
        self::assertEquals(200, $http->responseCode);

        $http = $this->execRoute(HttpMethods::GET, '/anything/but/not/api/hello');
        self::assertTrue($http->isHtml);
        self::assertEquals(200, $http->responseCode);

        self::assertTrue(true);
    }

    public function testDocument()
    {
        $doc = Router::document();
        self::assertIsArray($doc);
    }
}
