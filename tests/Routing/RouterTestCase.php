<?php

namespace Api\Routing;

use Api\TestCase;
use Api\Tools\Injector;

/**
 * Class RouterTestCase
 *
 * @package Api\Routing
 */
class RouterTestCase extends TestCase
{

    /**
     * @param string $method
     * @param string $uri
     * @param array  $query
     * @param array  $body
     * @param string $bearer
     *
     * @return HttpMock
     */
    public function execRoute(string $method, string $uri, array $query = [], array $body = [], string $bearer = ''): HttpMock
    {
        $http = new HttpMock();
        Injector::http($http);
        Router::forward($method, $uri, $query, $body, $bearer);
        return $http;
    }
}
