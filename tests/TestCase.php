<?php

namespace Api;

use Api\Tools\Injector;
use Exception as BaseException;

/**
 * Class TestCase
 *
 * @package LiliTools
 */
class TestCase extends \PHPUnit\Framework\TestCase
{

    /**
     * @param $expected
     * @param $actual
     */
    public static function assertSerialized($expected, $actual)
    {
        $actual   = json_decode(json_encode($actual), true);
        $expected = json_decode(json_encode($expected), true);
        self::assertEquals($expected, $actual);
    }

    /**
     * @param $expected
     * @param $actual
     */
    public static function assertArray($expected, $actual)
    {
        self::assertEquals(json_decode(json_encode($expected), true), json_decode(json_encode($actual), true));
    }

    /**
     * {@inheritdoc}
     *
     */
    public function setUp(): void
    {
        parent::setUp();

        //Set test date
        //Carbon::setTestNow('2019-07-17');
        //self::assertTrue(Carbon::hasTestNow());
        //self::assertEquals(1563321600, Carbon::now()->getTimestamp());

        //Attente active sur le conteneur mysql ...
        for ($i = 1; $i < 5; $i++) {
            try {
                Injector::database()->getDatabaseName();
                break;
            } catch (BaseException $exception) {
                sleep(5);
            }
        }

        //Check MySQL connection
        if (Injector::database()->getDatabaseName() !== 'ApiTestDatabase') {
            throw new BaseException('database not well configured');
        }

        //Execute MySQL migrations
        Injector::database()->exec(file_get_contents(__DIR__ . '/migration.sql'));
    }
}
