<?php

namespace Api\Tools;

use Api\Models\Ping;
use Api\TestCase;

/**
 * Class DBTest
 *
 * @issue https://gitlab.com/dev-packages-2017-2020/framework-php-api/-/issues/14 -- Test -- Improve code coverage for class DB
 * @package Api\Tools
 */
class DBTest extends TestCase
{
    public function testErrors()
    {
        Injector::database()->insert(
            'INSERT INTO Users(uid, name, email, hash, token) 
            VALUES (:uid, :name, :email, :hash, :token)',
            [
                'uid'   => '123',
                'name'  => 'tartempion',
                'email' => 'tartempion@mail.com',
                'hash'  => '...',
                'token' => '...A'
            ]
        );
        Injector::database()->insert(
            'INSERT INTO Users(uid, name, email, hash, token) 
            VALUES (:uid, :name, :email, :hash, :token)',
            [
                'uid'   => '456',
                'name'  => 'tartempion2',
                'email' => 'tartempion2@mail.com',
                'hash'  => '...',
                'token' => '...B'
            ]
        );
        Injector::database()->insert(
            'INSERT INTO Users(uid, name, email, hash, token) 
            VALUES (:uid, :name, :email, :hash, :token)',
            [
                'uid'   => '8910',
                'name'  => 'tartempion3',
                'email' => 'tartempion3@mail.com',
                'hash'  => '...',
                'token' => '...C'
            ]
        );
        Injector::database()->insert(
            'INSERT INTO Users(uid, name, email, hash, token) 
            VALUES (:uid, :name, :email, :hash, :token)',
            [
                'uid'   => '13',
                'name'  => 'tartempion4',
                'email' => 'tartempion4@mail.com',
                'hash'  => '...',
                'token' => '...D'
            ]
        );

        $foundRows = 0;
        $uidColumn = Injector::database()->selectColumn('SELECT uid FROM Users LIMIT 3', [], $foundRows);
        self::assertEquals([
            '123',
            '456',
            '8910',
        ], $uidColumn);
        self::assertEquals(4, $foundRows);

        try {
            Injector::database()->selectColumn(
                'SELECT uid FROM Users WHERE uid = :uid LIMIT 3',
                ['uid' => '13', 'invalid' => 12],
                $foundRows
            );
            self::fail();
        } catch (ApiException $exception) {
        }

        try {
            Injector::database()->selectOneOrFail('SELECT * FROM NotExist', Ping::class);
            self::fail();
        } catch (ApiException $exception) {
        }

        try {
            Injector::database()->insert(
                'INSERT INTO Users(uid, name, email, hash, token)
            VALUES (:uid, :name, :email, :hash, :token)',
                [
                    'uid'   => '13',
                    'name'  => 'tartempion4',
                    'email' => 'tartempion4@mail.com',
                    'hash'  => '...',
                    'token' => '...D'
                ]
            );
            self::fail();
        } catch (ApiException $exception) {
            self::assertEquals('Duplicate entry \'tartempion4@mail.com\' for key \'Users_email_uindex\'', $exception->getMessage());
        }

        try {
            Injector::database()->update(
                'UPDATE Users SET token = \'...A\' WHERE uid = \'13\''
            );
            self::fail();
        } catch (ApiException $exception) {
            self::assertEquals('Duplicate entry \'...A\' for key \'Users_token_uindex\'', $exception->getMessage());
        }
    }
}
