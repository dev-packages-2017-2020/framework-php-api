<?php

namespace Api\Tools;

use Api\TestCase;

/**
 * Class DirectoryVisitorTest
 *
 * @package Api\Tools
 */
class DirectoryVisitorTest extends TestCase
{
    public function testWalkFlat()
    {
        $content = '';
        $files   = [];
        DirectoryVisitor::walkFlat(
            __DIR__ . '/dir_to_visit',
            function ($fileName, $directory) use (&$content, &$files) {
                $content .= file_get_contents($directory . '/' . $fileName);
                $files[] = $fileName;
            }
        );
        self::assertArray([
            'test.txt',
            'test2.txt',
        ], $files);
        self::assertEquals('testtest2', $content);
    }

    public function testWalkRecursive()
    {
        $content = '';
        $files   = [];
        DirectoryVisitor::walkRecursive(
            __DIR__ . '/dir_to_visit',
            function ($fileName, $directory) use (&$content, &$files) {
                $content .= file_get_contents($directory . '/' . $fileName);
                $files[] = $fileName;
            }
        );
        self::assertArray([
            'sub_file_1.txt',
            'sub_file_2.txt',
            'sub_file_42',
            'test.txt',
            'test2.txt',
        ], $files);
        self::assertEquals('sub_file_1sub_file_242testtest2', $content);
    }
}
