USE ApiTestDatabase;

SET FOREIGN_KEY_CHECKS = 0;

drop table if exists Users;

create table Users (
    id    int auto_increment
        primary key,
    uid   varchar(255) not null,
    name  varchar(255) not null,
    email varchar(255) not null,
    hash  varchar(255) not null,
    token varchar(255) not null,
    constraint Users_email_uindex
        unique (email),
    constraint Users_name_uindex
        unique (name),
    constraint Users_token_uindex
        unique (token)
);

SET FOREIGN_KEY_CHECKS = 1;

